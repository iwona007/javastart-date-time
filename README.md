# javastart-date-time

Napisz prosty stoper.

Ex1
Wyświetl na ekranie komunikat o tym, że w celu rozpoczęcia pomiaru należy 
wcisnąć Enter. Następnie na ekranie pojawia się podobny komunikat, że w 
celu zatrzymania pomiaru należy ponownie wcisnąć Enter.

Na końcu wyświetl czas jaki udało Ci się zmierzyć w sekundach.

Skorzystaj ze Scannera i metody nextLine() w celu wstrzymania programu 
aż do czasu wciśnięcia Entera. Do zmierzenia czasu wykorzystaj klasy Instant oraz Duration.


Ex2
Napisz program, w którym wczytasz od użytkownika dwie daty, a następnie zrealizuj następujące podpunkty:

    wyświetl informację o tym, która data jest "późniejsza",
    wyświetl liczbę lat, miesięcy i dni pomiędzy wprowadzonymi datami.
W przypadku, gdy druga data jest "wcześniejsza" niż pierwsza, liczby dni,
miesięcy i lat również powinny być podane jako liczby dodatnie.
W rozwiązaniu wykorzystaj klasy LocalDate i Period.
