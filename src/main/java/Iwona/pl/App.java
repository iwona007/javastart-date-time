package Iwona.pl;

import java.time.LocalDate;

import static Iwona.pl.Dates.readDataFromUser;

public class App {
    public static void main(String[] args) {

        Time time = new Time();
        time.printDifferenceOFTime();

        LocalDate date1 = readDataFromUser();
        LocalDate date2 = readDataFromUser();
        Dates.showDaysBetween(date1, date2);
        Dates.showLaterDate(date1, date2);
    }
}
