package Iwona.pl;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class Dates {
    private static Scanner scanner = new Scanner(System.in);

    public static LocalDate readDataFromUser() {
        System.out.println("Podaj dzień miesiąca");
        int day = scanner.nextInt();
        System.out.println("Podaj miesiąc");
        int month = scanner.nextInt();
        System.out.println("Podaj rok");
        int year = scanner.nextInt();
        return LocalDate.of(year, month, day);
    }

    public static void showDaysBetween(LocalDate date1, LocalDate date2) {
        Period between = Period.between(date1, date2);
        System.out.println("Liczba dni pomiędzy datami: " + Math.abs(between.getDays()));
        System.out.println("Liczba miesięcy pomiędzy datami: " + Math.abs(between.getMonths()));
        System.out.println("Liczba lat pomiędzy datami: " + Math.abs(between.getYears()));
    }

    public static void showLaterDate(LocalDate date1, LocalDate date2) {
        LocalDate laterDate = date1.isAfter(date2) ? date1 : date2;
        System.out.println("Późniejsza data " + laterDate);
    }
}
