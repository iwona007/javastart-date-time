package Iwona.pl;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class Time {
    Scanner input = new Scanner(System.in);

    public void printDifferenceOFTime() {
        System.out.println("Żeby rozpocząć pomiar czasu wciśnij ENTER");
        input.nextLine();
        Instant time1 = Instant.now();

        System.out.println("Żeby zatrzymać pomiar czasu wciśnij ENTER");
        input.nextLine();
        Instant time2 = Instant.now();

        Duration time = Duration.between(time1, time2);
        System.out.println("Czas, który upłynął: " + time.getSeconds());
    }

}
